import './App.scss';
import CardList from './components/CardList/CardList';
import CardDetail from './components/CardDetail/CardDetail';
import Navbar from './components/Navbar/Navbar';
import CardsProvider from './store/CardsProvider';
import ModalProvider from './store/ModalProvider';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import About from './components/About/About';
import { ROUTES } from './globals/constants';

function App() {
  return (
    <div className="App">
      <Router basename={process.env.PUBLIC_URL}>
        <Navbar />
        <Switch>
          <Route exact path={ROUTES.home.path}>
            <ModalProvider>
              <CardsProvider>
                <CardList quantity={6} />
                <CardDetail />
              </CardsProvider>
            </ModalProvider>
          </Route>
          <Route path={ROUTES.about.path}>
            <About />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
