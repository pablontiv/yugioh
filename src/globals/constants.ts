import { CardProperty } from './types';

export const CARD_LIST_DEFAULT_QTY = 5;
export const CARD_LIST_BASE_URL = 'https://db.ygoprodeck.com/api/v7/cardinfo.php?attribute=dark';

export const CARD_PROPERTIES_MAP: CardProperty[] = [
  { name: 'type', alias: 'Type:' },
  { name: 'atk', alias: 'Attack:' },
  { name: 'def', alias: 'Defense:' },
  { name: 'level', alias: 'Level:' },
  { name: 'race', alias: 'Race:' },
  { name: 'attribute', alias: 'Attribute:' },
  { name: 'archetype', alias: 'Archetype:' },
  { name: 'scale', alias: 'Scale:' },
];

export const ROUTES = {
  home: {
    name: 'My Cards',
    path: '/',
  },

  about: {
    name: 'About me!',
    path: '/about',
  },
};
