export type CardListType = {
  quantity?: number;
};

export type CardImage = {
  id: number;
  image_url: string;
  image_url_small: string;
};

export type BaseCardType = {
  id: number;
  name: string;
  image: string;
};

export type CardType = {
  id: number;
  name: string;
  type: string;
  desc: string;
  atk: number;
  def: number;
  level: number;
  race: string;
  attribute: string;
  archetype: string;
  scale: number;
  card_images: CardImage[];
};

export type CardProperty = {
  name: string;
  alias: string;
};

export type CardsContextType = {
  cards: CardType[];
  selectCard(id: number): void;
  getSelectedCard(): CardType | undefined;
};

export type ModalContextType = {
  isVisible(): boolean;
  toggle(): void;
};
