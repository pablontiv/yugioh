import { FC, useContext } from 'react';
import { CARD_PROPERTIES_MAP } from '../../globals/constants';
import { CardsContext } from '../../store/CardsContext';
import { ModalContext } from '../../store/ModalContext';
import styles from './CardDetail.module.scss';

const CardDetail: FC = () => {
  const modalCtx = useContext(ModalContext);
  const cardsCtx = useContext(CardsContext);
  const currentCard = cardsCtx.getSelectedCard();
  const properties = currentCard
    ? CARD_PROPERTIES_MAP.filter((property) => Object.keys(currentCard).includes(property.name))
    : [];

  return modalCtx.isVisible() && currentCard ? (
    <div className={styles['modal-container']}>
      <div className={`${styles.modal} ${styles['is-active']}`}>
        <div className={styles['modal-background']}></div>
        <div className={styles['modal-content']}>
          <div className={styles['card-columns']}>
            <div className={styles.column}>
              <figure className={styles['card-figure']}>
                <img
                  className={styles['card-image']}
                  src={currentCard.card_images[0].image_url}
                  alt={currentCard.name}
                />
              </figure>
            </div>
            <div className={styles['card-column']}>
              <div className={styles.card}>
                <div className={styles['card-content']}>
                  <div className={styles.content}>
                    <div className={styles['card-title']}>{currentCard.name}</div>
                    <div className={styles['card-detail-container']}>
                      {properties.map((property) => {
                        return (
                          <div key={property.name} className={styles['card-detail']}>
                            <div className={styles['card-detail-title']}>{property.alias}</div>
                            <div>{Reflect.get(currentCard, property.name)}</div>
                          </div>
                        );
                      })}
                    </div>
                    <div className={styles['card-detail-title']}>Description:</div>
                    <div>{currentCard.desc}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <button className={styles['modal-close']} aria-label="close" onClick={() => modalCtx.toggle()}></button>
      </div>
    </div>
  ) : (
    <></> //TODO: Empty modal
  );
};

export default CardDetail;
