import { FC, useContext } from 'react';
import { BaseCardType } from '../../globals/types';
import { CardsContext } from '../../store/CardsContext';
import { ModalContext } from '../../store/ModalContext';
import styles from './Card.module.scss';

const Card: FC<BaseCardType> = ({ id, name, image }) => {
  const cardsCtx = useContext(CardsContext);
  const modalCtx = useContext(ModalContext);

  const clickHandler = () => {
    cardsCtx.selectCard(id);
    modalCtx.toggle();
  };

  return (
    <button className={styles.card} onClick={() => clickHandler()}>
      <img src={image} alt={name} />
    </button>
  );
};

export default Card;
