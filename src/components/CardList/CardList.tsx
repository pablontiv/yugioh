import { FC, useContext } from 'react';
import { CARD_LIST_DEFAULT_QTY } from '../../globals/constants';
import { CardListType } from '../../globals/types';
import { CardsContext } from '../../store/CardsContext';
import Card from '../Card/Card';
import styles from './CardList.module.scss';

const CardList: FC<CardListType> = ({ quantity }) => {
  quantity = quantity ?? CARD_LIST_DEFAULT_QTY;

  const cardsCtx = useContext(CardsContext);

  return (
    <div className={styles.list}>
      {cardsCtx.cards.map((card) => (
        <Card key={card.id} {...card} image={card.card_images[0].image_url_small} />
      ))}
    </div>
  );
};

export default CardList;
