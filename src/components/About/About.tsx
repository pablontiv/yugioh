import React from 'react';
import styles from './About.module.scss';

const About = () => {
  return (
    <div className={styles.about}>
      <h1 className={styles.title}>I'm Pablo(ntiv)</h1>
      <small className={styles.subtitle}>Bits artisan</small>
      <p className={styles.subtitle}>learning passionate and teaching enthusiast</p>
    </div>
  );
};

export default About;
