import { Link } from 'react-router-dom';
import { ROUTES } from '../../globals/constants';
import styles from './Navbar.module.scss';

const Navbar = () => {
  return (
    <nav className={styles.navbar}>
      <div className={styles['navbar-brand']}>
        <Link className={styles['navbar-item']} to={ROUTES.home.path}>
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Yu-Gi-Oh%21_%28Logo%29.jpg/320px-Yu-Gi-Oh%21_%28Logo%29.jpg"
            alt="My Yu Gi Oh! Cards"
          />
        </Link>
      </div>

      <div className={`${styles['navbar-menu']} `}>
        <div className={styles['navbar-start']}>
          <Link className={styles['navbar-item']} to={ROUTES.home.path}>
            {ROUTES.home.name}
          </Link>
          <Link className={styles['navbar-item']} to={ROUTES.about.path}>
            {ROUTES.about.name}
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
