import React from 'react';
import { CardsContextType, CardType } from '../globals/types';

const initialContext: CardsContextType = {
  cards: [],
  selectCard: () => {},
  getSelectedCard: () => ({} as CardType),
};

export const CardsContext = React.createContext(initialContext);
