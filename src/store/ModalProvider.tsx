import { useState } from 'react';
import { ModalContextType } from '../globals/types';
import { ModalContext } from './ModalContext';

const ModalProvider = (props: any) => {
  const [visible, setVisible] = useState(false);
  const toggleHandler = () => {
    setVisible(!visible);
  };

  const modalContext: ModalContextType = {
    isVisible: () => visible,
    toggle: toggleHandler,
  };

  return <ModalContext.Provider value={modalContext}>{props.children}</ModalContext.Provider>;
};

export default ModalProvider;
