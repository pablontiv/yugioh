import React from 'react';
import { ModalContextType } from '../globals/types';

const initialContext: ModalContextType = {
  isVisible: () => false,
  toggle: () => {},
};

export const ModalContext = React.createContext(initialContext);
