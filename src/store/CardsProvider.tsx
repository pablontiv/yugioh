import { useEffect, useState } from 'react';
import { CARD_LIST_BASE_URL } from '../globals/constants';
import { CardsContextType, CardType } from '../globals/types';
import { CardsContext } from './CardsContext';

const CardsProvider = (props: any) => {
  const [selectedCard, setSelectedCard] = useState(0);
  const [cards, setCards] = useState<CardType[]>([]);

  const fetchCards = async () => {
    try {
      //TODO: Randomize cards
      const url = `${CARD_LIST_BASE_URL}&num=6&offset=1`;
      const response = await fetch(url);
      const { data } = await response.json();
      setCards(data);
    } catch (error) {}
  };

  useEffect(() => {
    if (!cards.length) {
      fetchCards();
    }
  }, [cards]);

  const selectCardHandler = (id: number) => {
    setSelectedCard(id);
  };

  const getSelectedCardHandler = () => {
    return cards.find((card) => card.id === selectedCard);
  };

  const cardsContext: CardsContextType = {
    cards: cards,
    selectCard: selectCardHandler,
    getSelectedCard: getSelectedCardHandler,
  };

  return <CardsContext.Provider value={cardsContext}>{props.children}</CardsContext.Provider>;
};

export default CardsProvider;
