FROM node:alpine As development
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn
COPY . .
RUN yarn build

FROM nginx:alpine
COPY --from=development /app/build/ /usr/share/nginx/html/
